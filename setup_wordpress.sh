#!/usr/bin/env bash

if [ "$USER" = "root" ]; then
    echo 'You should not run this script as root.'; exit 1
fi

distro=$(awk -F= '/^ID=.*/ {print $2}' /etc/os-release)
if [ "$distro" = "ubuntu" ]; then
    url='https://ubuntu.com/tutorials/install-and-configure-wordpress#1-overview'
    xdg-open $url &
    disown
    echo "Follow the instructons at: $url"
    exit 0
fi

comment() {
    sudo sed -i "/^$1$/s/.*/$sym$1/" "$file"
}
uncomment() {
    sudo sed -i "/^$sym$1$/s/.*/$1/" "$file"
}

sudo pacman -S --noconfirm --needed yay base-devel unzip

# MariaDB/MySQL
yay -S --noconfirm --needed mysql
sudo mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
sudo systemctl enable --now mysql
sudo mysql -e "CREATE USER 'admin'@'localhost' IDENTIFIED BY 'password';"
sudo mysql -e "CREATE DATABASE wordpress;"
sudo mysql -e "GRANT ALL PRIVILEGES ON wordpress.* TO 'admin'@'localhost' IDENTIFIED BY 'password';"
sudo mysql -e "FLUSH PRIVILEGES;"

# PHP
yay -S --noconfirm --needed php xdebug
PHP_VERSION=$(php -v | grep -Eo '([0-9]*\.?)*' | awk -F. '{print $1"."$2; exit}')
PHP_VERSION_MAJOR="$(echo "${PHP_VERSION}" | awk -F. '{print $1}')"
PHP_VERSION_MINOR="$(echo "${PHP_VERSION}" | awk -F. '{print $2}')"

file='/etc/php/conf.d/xdebug.ini'
sym=';'
uncomment 'zend_extension=xdebug'
uncomment 'xdebug.remote_enable=on'
uncomment 'xdebug.remote_host=127.0.0.1'
uncomment 'xdebug.remote_port=9000'
uncomment 'xdebug.remote_handler=dbgp'

extension='zend_extension=xdebug'
if ! grep -iq "${extension}" "$file"; then
    echo "${extension}" | sudo tee -a "$file"
fi

option="xdebug.mode=debug"
if ! grep -iq "${option}" "$file"; then

    echo "xdebug.mode=debug" | sudo tee -a "$file"
fi

if ! php -v | grep -iq 'with xdebug'; then
    echo 'Xdebug not enabled.'
fi

# Fixes 'Call to undefined function mysql_connect()'
file='/etc/php/php.ini'
sym=';'
uncomment 'extension=pdo_mysql'
uncomment 'extension=mysqli'

# Apache
yay -S --noconfirm --needed apache php-apache
file='/etc/httpd/conf/httpd.conf'
sym='#'
comment 'LoadModule mpm_event_module modules\/mod_mpm_event.so'
uncomment 'LoadModule mpm_prefork_module modules\/mod_mpm_prefork.so'
uncomment 'LoadModule rewrite_module modules\/mod_rewrite.so'

# As of PHP version 8.x, the PHP module for Apache has been renamed.
php_module="$(find /etc/httpd/modules/ -iname 'libphp*so' | awk -F/ '{print $NF}')"
if [ "${PHP_VERSION_MAJOR}" -ge '8' ]; then
    sudo tee -a /etc/httpd/conf/httpd.conf << EOF
LoadModule php_module modules/${php_module}
EOF
else
    sudo tee -a /etc/httpd/conf/httpd.conf << EOF
LoadModule php${PHP_VERSION_MAJOR}_module modules/${php_module}
EOF
fi

sudo tee -a /etc/httpd/conf/httpd.conf << EOF
AddHandler php-script .php
Include conf/extra/php_module.conf
EOF

sudo systemctl start httpd
sudo systemctl enable httpd

# WordPress
yay -S --noconfirm --needed wordpress phpmyadmin
yay -S --noconfirm --needed wp-cli

sudo tee /etc/httpd/conf/extra/httpd-wordpress.conf << EOF
<VirtualHost *:80>
	DocumentRoot /usr/share/webapps/wordpress
	<Directory /usr/share/webapps/wordpress>
		Options +Indexes +FollowSymLinks +ExecCGI
          	AllowOverride All
          	Order deny,allow
          	Allow from all
          	Require all granted
	</Directory>
</VirtualHost>
EOF

sudo tee -a /etc/httpd/conf/httpd.conf << EOF
Include conf/extra/httpd-wordpress.conf
EOF

sudo systemctl restart httpd
sudo systemctl enable --now mysqld

# Fixes 'Plugins are unable to install: Could not create directory'
sudo groupadd wp
sudo usermod -a -G wp http
sudo usermod -a -G wp "$USER"
sudo chown "$USER":"$USER" -R /usr/share/webapps/wordpress
sudo chmod -R 774 /usr/share/webapps/wordpress 

pushd /usr/share/webapps/wordpress

# Fixes 'Cannot save plugins to localhost'
file='/usr/share/webapps/wordpress/wp-config.php'
if [ -f $file ]; then
    sudo sed -Ei "/^(\s*|#*\s*|\/{2,}\s*)?define\('FS_METHOD'.*/s/.*/define('FS_METHOD', 'direct');/" $file
else
    wp config create --dbname='wordpress' --dbuser='admin' --dbpass='password' --skip-check
    sudo tee -a $file << EOF
define('FS_METHOD', 'direct');
EOF
fi

wp core install --url=localhost --title='WordPress' --admin_user='admin' --admin_password='password' --admin_email='youremail@example.com' --skip-email
popd

sudo chown http:wp -R /usr/share/webapps/wordpress

xdg-open 'http://localhost' >/dev/null 2>&1 &
disown
